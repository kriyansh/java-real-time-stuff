package com.pawan.app;

public class StudentNotFoundException extends Exception{

	public StudentNotFoundException(String errorMessage) {
		super(errorMessage);
	}
}

package com.pawam.files;

public class ExceptionDemo {
	//dev1 of team1
	public boolean checkEligibility(int age) throws NotEligibleException {
		
		boolean isEligible = false;
		
		if(age < 18) {
			System.out.println("Not Eligible");
			
			throw new NotEligibleException("Not eligible to vote");
			
		}else {
			isEligible = true;
			System.out.println("Eligible");
		}
		
		return isEligible;
	}

	//dev2 of team2
	//dev of team3
	public static void main(String[] args) {
		
		int age = 16;
		
		ExceptionDemo exDemo = new ExceptionDemo();
		
			boolean isEligible;
			try {
				isEligible = exDemo.checkEligibility(age);
			} catch (NotEligibleException e) {
				System.out.println(e.getMessage());
			}
			
		
	}

}

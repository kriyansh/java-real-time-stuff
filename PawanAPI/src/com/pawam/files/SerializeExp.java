package com.pawam.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Student implements Serializable{
	
	private static final long serialVersionUID = -6144580706546066843L;

	private String name;
	
	private static int age;
	
	private String email;
	
	private transient String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		String data = String.format("Name : %s, Age: %s, Email: %s, password: %s", name, age, email, password);
		return data;
	}
	
}

public class SerializeExp {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Student student = new Student();
		student.setAge(22);
		student.setEmail("user@gmail.com");
		student.setName("Test user");  
		student.setPassword("13654237");
		
		String objectFile = "F:\\files\\student.obj";
		
		//serialization start
		
		
		FileOutputStream fos = new FileOutputStream(new File(objectFile));
		ObjectOutputStream outStream = new ObjectOutputStream(fos);
		
		outStream.writeObject(student);
		
		student.setAge(30);
		
		System.out.println(student);
		
		System.out.println("Object saved..");
		
		
		
		//serialization end
		
		FileInputStream fis = new FileInputStream(new File(objectFile));
		
		ObjectInputStream inputStream = new ObjectInputStream(fis);
		
		Student student2 = (Student) inputStream.readObject();
		
		System.out.println("Student data after deserialization : " + student2);
	}
}
